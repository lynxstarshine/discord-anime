import { TYPES } from './utils/types';
import container from "./utils/inversify.config"
import { BotService } from './service/Bot';
import { Logger } from 'winston';
import { JellyfinAPI } from './service/Jellyfin';

const bot = container.get<BotService>(TYPES.Bot)
const logger = container.get<Logger>(TYPES.Logger)

const jellyfinApi = container.get<JellyfinAPI>(TYPES.JellyfinAPI)

logger.info("starting")

bot.init()
