import "reflect-metadata"

import { Container } from "inversify"
import { TYPES } from "./types"

import logger from '../service/logger'
import { AnimesyncServer } from "../service/AnimesyncServer"
import { BotService } from "../service/Bot"
import { AnimesyncController } from "../service/AnimeSyncController"
import { JellyfinAPI } from "../service/Jellyfin"
import { AnimeSyncQueue } from "../service/AnimeSyncQueue"

const container = new Container()

container.bind(TYPES.Logger).toConstantValue(logger)
container.bind(TYPES.AnimeSyncServer).to(AnimesyncServer).inSingletonScope()
container.bind(TYPES.AnimeSyncController).to(AnimesyncController).inSingletonScope()
container.bind(TYPES.Container).toConstantValue(container)
container.bind(TYPES.Bot).to(BotService).inSingletonScope()
container.bind(TYPES.JellyfinAPI).to(JellyfinAPI).inSingletonScope()
container.bind(TYPES.AnimeSyncQueue).to(AnimeSyncQueue).inSingletonScope()

export default container;
