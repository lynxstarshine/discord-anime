export const TYPES = {
    Logger: Symbol.for('Logger'),
    AnimeSyncServer: Symbol.for('AnimesyncServer'),
    AnimeSyncController: Symbol.for('AnimeSyncController'),
    AnimeSyncQueue: Symbol.for('AnimeSyncQueue'),
    Container: Symbol.for('Container'),
    Bot: Symbol.for('Bot'),
    JellyfinAPI: Symbol.for('JellyfinAPI')
}
