import { Interaction, RESTPostAPIApplicationCommandsJSONBody, SlashCommandOptionsOnlyBuilder } from "discord.js";
import { injectable } from "inversify";

export interface ICommand {
    info: RESTPostAPIApplicationCommandsJSONBody
    execute(interaction: Interaction): Promise<void>;
}

export interface ICommandConstructor {
    new (...args:unknown[]): ICommand
}

@injectable()
export abstract class AbstractCommand implements ICommand {
    abstract info: RESTPostAPIApplicationCommandsJSONBody;
    abstract execute(interaction: Interaction): Promise<void>;
}
