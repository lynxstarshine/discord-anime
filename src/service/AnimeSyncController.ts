import { inject, injectable } from "inversify";
import { TYPES } from "../utils/types";
import { AnimesyncServer } from "./AnimesyncServer";
import { checkAsync, Server, api as AnimeSyncAPI, Options } from "animesync/dist/shell"
import { CacheType, DiscordAPIError, EmbedBuilder, Interaction } from "discord.js"

import fs from 'fs';
import path from 'path';

import { Logger } from "winston";
import { BotService } from "./Bot";
import { AnimeSyncQueue } from "./AnimeSyncQueue";


interface CurrentStatusDownload {
  url: string
  serie: {
    title: string,
    synopsis?: string
  }

  currentSeason?: {
    title?: string,
    number: number
  }
  currentEpisode?: {
    title?: string,
    number: number
  }
}


async function tryUpdateAsync(api: Server, series: AnimeSyncAPI.LibraryContextSeries, sourceUrl?: string, options?: Options) {
  if (sourceUrl || options?.skipUpdate) return;
  api.logger.info(`Updating ${series.title}`);
  await api.library.seriesPutAsync({ seriesId: series.id });
}


@injectable()
export class AnimesyncController {

  @inject(TYPES.AnimeSyncServer) animeSyncServer!: AnimesyncServer
  @inject(TYPES.Logger) logger!: Logger
  @inject(TYPES.Bot) bot!: BotService
  @inject(TYPES.AnimeSyncQueue) queue!: AnimeSyncQueue

  private _isDownloading = false;

  private _currentDownload?: CurrentStatusDownload = undefined

  get isDownloading(): boolean {
    return this._isDownloading;
  }

  get currentDownload(): CurrentStatusDownload | undefined {
    return this._currentDownload
  }

  get currentDownloadMessage() {
    if (!this._currentDownload) {
      return undefined
    }

    const embed = new EmbedBuilder()

    embed.setAuthor({ name: `Downloading ${this._currentDownload.serie.title}` }).setTitle(this._currentDownload.serie.title)

    if (this._currentDownload.serie.synopsis) {
      embed.setDescription(this._currentDownload.serie.synopsis)
    }

    if (this._currentDownload.currentSeason) {
      let value = `Season ${this._currentDownload.currentSeason.number}`

      if (this._currentDownload.currentSeason.title) {
        value += `: ${this._currentDownload.currentSeason.title}`
      }
      embed.addFields([
        {name: 'Currently downloading season', value: value}
      ])
    }

    if (this._currentDownload.currentEpisode) {
      let value = `Episode ${this._currentDownload.currentEpisode.number}`

      if (this._currentDownload.currentEpisode.title) {
        value += `: ${this._currentDownload.currentEpisode.title}`
      }
      embed.addFields([
        {name: 'Currently downloading episode', value: value}
      ])
    }

    return embed
  }

  async sendUpdateInteraction(interaction: Interaction<CacheType>) {
    try {
      if (interaction.isRepliable()) {
        await interaction.editReply({
          content: 'Downloading anime',
          embeds: [this.currentDownloadMessage!.data]
        })
      }
    } catch (e) {
      if (e instanceof DiscordAPIError) {
        return
      }

      throw e
    }
  }

  async download(url: string, interaction: Interaction<CacheType>) {
    if (this.isDownloading) {
      this.logger.info('Already download a serie you need to pause the current serie before starting a new download')
      return
    }

    this._isDownloading = true

    const api = await this.animeSyncServer.getApi()

    await checkAsync(api, [url], async (series: AnimeSyncAPI.LibraryContextSeries, sourceUrl?: string) => {
      await tryUpdateAsync(api, series, sourceUrl);
      const context = await api.library.seriesAsync({ seriesId: series.id });

      if (context.value) {
        this._currentDownload = {serie: {title: context.value.title}, url: url}
        this.sendUpdateInteraction(interaction)
      }

      const seasons = context.value?.seasons ?? [];
      seasons.sort((a, b) => a.season - b.season)

      for (const season of seasons) {
        const seasonName = path.basename(season.path);
        season.episodes.sort((a, b) => a.episode - b.episode)

        this._currentDownload!.currentSeason = { number: season.season, title: season.title}

        // this.sendUpdateInteraction(interaction)

        for (const episode of season.episodes) {
          this._currentDownload!.currentEpisode = { number: episode.episode, title: episode.title }
          this.sendUpdateInteraction(interaction)

          const episodeName = path.basename(episode.path);

          if (episode.title == "PV") {
            this.logger.info(`Skip promotional video: ${episodeName}`)
            continue
          }
          const trackerPath = path.join(series.path, '.animesync', seasonName, episodeName);
          const tracked = await fs.promises.access(trackerPath).then(() => true, () => false);
          if (episode.active || episode.available || tracked) {
            this.logger.info(`Skipping ${episodeName}`);
          } else if (episode.url) {
            this.queue.add({
              download: {seriesId: series.id, episodeId: episode.id},
              episodeName: episodeName,
              trakerPath: trackerPath,
              episodePath: episode.path
            })
          }
        }
      }
    })

    this._isDownloading = false;
    this._currentDownload = undefined
  }
}
