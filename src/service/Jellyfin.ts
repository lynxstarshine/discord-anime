import { Api, Jellyfin } from "@jellyfin/sdk"
import { getSystemApi } from "@jellyfin/sdk/lib/utils/api/system-api"
import { inject, injectable } from "inversify"
import { config } from "node-config-ts"
import { Logger } from "winston"
import { TYPES } from "../utils/types"


@injectable()
export class JellyfinAPI {
  protected jellyfin 

  readonly api;

  constructor(@inject(TYPES.Logger) private logger: Logger) {
    this.logger.info("Init Jellyfin API")

    this.jellyfin = new Jellyfin({
      clientInfo: {
          name: 'Animesync Bot',
          version: '0.1.0'
      },
      deviceInfo: {
          name: 'Animesync Bot',
          id: 'animesync-bot'
      }
    })

    this.api = this.jellyfin.createApi(config.jellyfin.server, config.jellyfin["api-key"])

    getSystemApi(this.api).getSystemInfo().then(response => {
      this.logger.info(`Jellyfin server connected to ${response.data.ServerName}`)
    }, (reason) => {
      this.logger.warn('Jellyfin API error', reason)
    })
  }
}
