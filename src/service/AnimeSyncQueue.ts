import { inject, injectable } from "inversify";
import { TYPES } from "../utils/types";
import { AnimesyncServer } from "./AnimesyncServer";
import { config } from "node-config-ts"

import Keyv from "keyv"
import { BotService } from "./Bot";

type QueueInfo = {
  trakerPath: string,
  episodePath: string,
  episodeName: string,
  download: {seriesId: string, episodeId: string}
}

import fs from 'fs';
import path from 'path';
import { Logger } from "winston";

async function trackAsync(trackerPath: string) {
  const dirPath = path.dirname(trackerPath);
  await fs.promises.mkdir(dirPath, { recursive: true });
  await fs.promises.writeFile(trackerPath, Buffer.alloc(0));
}


@injectable()
export class AnimeSyncQueue {

  protected storage = new Keyv(config.keyv, {namespace: "anime-queue"})

  protected queue: QueueInfo[];

  protected initialized = false;

  protected _isRunning = false;

  protected _currentTask? : QueueInfo = undefined

  constructor(
    @inject(TYPES.AnimeSyncServer) protected animeSyncServer: AnimesyncServer,
    @inject(TYPES.Bot) protected bot: BotService,
    @inject(TYPES.Logger) private logger: Logger) {
      this.queue = []

      this.init()
  }

  async init() {

    const dbQueue : QueueInfo[] =  await this.storage.get("queue", {raw: false});

    if (dbQueue) {
      this.queue = dbQueue
    }

    this.runner()
  }

  add(episode: QueueInfo) {
    this.queue.push(episode)
    this.storage.set("queue", this.queue)

    this.runner()
  }

  protected get nextTaskInfo() {
    const task = this.queue.shift()
    return task
  }

  get isRunning() {
    return this._isRunning
  }

  async runner() {

    return new Promise<void>(async (resolve, reject) => {
      if (this._isRunning) {
        this.logger.debug("Queue already running")
        return
      }
  
      const task = this.nextTaskInfo
  
      this._currentTask = task
  
      if (task === undefined) {
        this.logger.info("Animesync queue is empty")
        return
      }
      this._isRunning = true
  
      const api = await this.animeSyncServer.getApi()
  
      this.logger.info(`Fetching ${task.episodeName}`);
      if (await api.library.episodePutAsync(task.download).then(x => x.success)) {
        await trackAsync(task.trakerPath);
        this.logger.info(`Finish downlading ${task.episodeName}`)
      } else {
        this.logger.warn(`Rejected ${path.basename(task.episodePath)}`);
      }

      this.storage.set("queue", this.queue)
      this._isRunning = false;
      this._currentTask = undefined
      resolve()

      this.runner()
    })
  }
}
