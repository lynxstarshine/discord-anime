import { CacheType, Client, Collection, GatewayIntentBits, Interaction, } from "discord.js"
import { injectable, inject, Container } from "inversify"
import { Logger } from "winston"
import { TYPES } from "../utils/types"

import { config } from "node-config-ts"
import { ICommand, ICommandConstructor } from "../utils/Command"

import fs from "node:fs"
import path from "node:path"


@injectable()
export class BotService {

  protected logger: Logger

  readonly client: Client
  protected container: Container

  protected commands: Collection<string, ICommand>
  private commandsInitialized = false

  constructor(@inject(TYPES.Logger) logger: Logger, @inject(TYPES.Container) container: Container) {
    this.logger = logger
    this.container = container
    this.client = new Client({
      intents: [
        GatewayIntentBits.Guilds
      ]
    })

    this.commands = new Collection()
  }

  init() {
    this.logger.info('Init Discord Bot')

    this._loadCommands()

    this.client.once('ready', () => {
      this.logger.info('Discord connection ready')
    })

    this.client.on('interactionCreate', async (interaction: Interaction<CacheType>) => {
      if (!interaction.isChatInputCommand()) {
        return
      }

      this.logger.debug(`Command ${interaction.commandName} received`)

      const command = this.commands.get(interaction.commandName)

      if (command === undefined) {
        this.logger.warn(`Command ${interaction.commandName} not found`)
        await interaction.reply(`Unknown command: ${interaction.commandName} !`)
        return
      }

      this.logger.debug(`Execute command ${interaction.commandName}`)
      command.execute(interaction);
    })

    this.client.login(config.discord.token)
  }

  _loadCommands() {
    if (this.commandsInitialized) {
      this.logger.warn('Commands already initialized !')
      return
    }

    const commandsPath = path.join(__dirname, '..', 'commands')
    const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'))

    commandFiles.forEach(file => {
      const filePath = path.join(commandsPath, file)
      const commandClass: ICommandConstructor = require(filePath).default

      const command = this.container.resolve(commandClass)
      this.commands.set(command.info.name, command)

      this.logger.info(`Command ${command.info.name} registered`)
    })
  }
}
