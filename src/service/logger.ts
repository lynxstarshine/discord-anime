import { createLogger, transports, format } from "winston"

const logger = createLogger({
  level: 'debug',
  defaultMeta: {
    service: 'animesync-bot'
  },
  transports: [
    new transports.Console({
      format: format.combine(
        format.colorize(),
        format.simple()
      ),
    })
  ]
})

export default logger;