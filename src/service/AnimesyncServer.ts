import * as AnimeSync from "animesync/dist/server"
import { inject, injectable } from "inversify";
import { Logger } from "winston";
import { TYPES } from "../utils/types";

@injectable()
export class AnimesyncServer {
  private _api?: AnimeSync.Server
  private apiPromiseResolver!: (value: void | PromiseLike<void>) => void;

  constructor(@inject(TYPES.Logger) private logger: Logger) {
    process.on("SIGINT", () => {
      this.close()
      process.exit()
    })
  }

  private async startServer() {
    let globalResolver: (value: void | PromiseLike<void>) => void;

    this.logger.info('Starting Animesync server')
    const returnPromise = new Promise<void>((resolver) => {
      globalResolver = resolver
    })
    const serverPromise = AnimeSync.Server.usingAsync((api) => {
      this._api = api
      this.logger.info('Animesync server started')
      globalResolver();
      return new Promise<void>((resolver) => {

        this.apiPromiseResolver = resolver;
      })
    })

    serverPromise.then(() => {
      this._api = undefined
      this.logger.info(`Animesync server stopped`)
    })

    return returnPromise;
  }

  close() {
    if (this._api === undefined) {
      this.logger.debug('Animesync server already closed')
      return
    }

    this._api = undefined;
    this.logger.info('Stop animesync server')
    this.apiPromiseResolver()
  }

  async getApi(): Promise<AnimeSync.Server> {
    if (this._api === undefined) {
      await this.startServer()
    }

    return this._api!
  }
}