import {config} from "node-config-ts"

import {REST, RESTPostAPIApplicationCommandsJSONBody, Routes} from "discord.js"

import fs from "node:fs"
import path from "node:path"

import container from "./utils/inversify.config"
import { ICommandConstructor, ICommand } from "./utils/Command"

const commands: RESTPostAPIApplicationCommandsJSONBody[] = []

const commandsPath = path.join(__dirname, 'commands')
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'))

commandFiles.forEach(file => {
    const filePath = path.join(commandsPath, file)
    const commandClass: ICommandConstructor = require(filePath).default

    commands.push(container.resolve(commandClass).info)
})

console.log(JSON.stringify(commands, null, 4))


const rest = new REST({ version: '10' }).setToken(config.discord.token);

rest.put(Routes.applicationCommands(config.discord.client_id), { body: commands })
	.then(() => console.log('Successfully registered application commands.'))
	.catch(console.error);
