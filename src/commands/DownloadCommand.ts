import { inject, injectable } from "inversify";
import { AbstractCommand } from "../utils/Command";
import { Logger } from "winston"
import { TYPES } from "../utils/types";
import { CacheType, Interaction, SlashCommandBuilder, SlashCommandStringOption } from "discord.js";
import { AnimesyncController } from "../service/AnimeSyncController";

@injectable()
export default class DownloadCommand extends AbstractCommand {

    @inject(TYPES.AnimeSyncController) animesyncController!: AnimesyncController
    @inject(TYPES.Logger) logger!: Logger

    info = (new SlashCommandBuilder().setDescription('Download an anime').setName('download').addStringOption(option => option.setName('url').setDescription('URl of serie to download (Currently only crunchyroll supported)'))).toJSON()

    async execute (interaction: Interaction<CacheType>) {
        if (!interaction.isChatInputCommand()) {
            return
        }

        if (this.animesyncController.isDownloading) {

            await interaction.reply({
                content: 'Downlading anime',
                embeds: this.animesyncController.currentDownload ? [this.animesyncController.currentDownloadMessage!.data] : undefined
            })

            return;
        }

        let url = interaction.options.getString('url')

        if (url) {
            interaction.deferReply()
            this.animesyncController.download(url, interaction)

        } else {
            await interaction.reply("You need to provide an url !")
            return
        }
    };
}
