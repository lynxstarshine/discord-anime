/* tslint:disable */
/* eslint-disable */
declare module "node-config-ts" {
  interface IConfig {
    discord: Discord
    keyv: string
    jellyfin: Jellyfin
  }
  interface Jellyfin {
    server: string
    "api-key": string
  }
  interface Discord {
    token: string
    client_id: string
  }
  export const config: Config
  export type Config = IConfig
}
